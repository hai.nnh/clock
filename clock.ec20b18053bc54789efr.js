const setTimeKeeper = () => {
  let date = new Date,
    _hour = date.getHours(),
    _min = date.getMinutes(),
    _sec = date.getSeconds();

  $('.keeper .sec-num').html(_sec)
  const secDeg = 90 + 6 * _sec
  $('.keeper.sec').css("transform", `rotate(${secDeg}deg)`)
  $('.keeper.sec .sec-num').css("transform", `rotate(${secDeg >= 270 ? 0 : 180}deg)`)
  // $('.clock').css("background", `linear-gradient(${secDeg - 90}deg, #ff7373, #fdfdfd)`)

  $('.keeper .min-num').html(_min)
  const minDeg = 90 + 6 * _min + (_sec / 10)
  $('.keeper.min').css("transform", `rotate(${minDeg}deg)`)
  $('.keeper.min .min-num').css("transform", `rotate(${minDeg >= 270 ? 0 : 180}deg)`)

  $('.keeper .hour-num').html(_hour)
  const hourDeg = 90 + 30 * (_hour % 12) + (_min / 2)
  $('.keeper.hour').css("transform", `rotate(${hourDeg}deg)`)
  $('.keeper.hour .hour-num').css("transform", `rotate(${hourDeg >= 270 ? 0 : 180}deg)`)
}

const generateSecSlash = () => {
  for (let i = 0; i < 30; i++) {
    const isHourSlash = (i % 5 == 0)
    $('.clock-inner').append(`<span class="sec-slash ${isHourSlash ? 'hour-slash' : ''}" style="transform:rotate(${i * 6}deg)"></span>`)
  }
}

const generateHourSlashInner = () => {
  for (let i = 0; i < 6; i++) {
    const doubleSlash = (i == 3)
    $('.clock-field').append(`<span class="hour-slash-inner ${doubleSlash ? 'is-double' : ''}" style="transform:rotate(${i * 30}deg)"></span>`)
  }
}

const changeBackground = () => {
  let date = new Date,
    _hour = date.getHours();

  if (_hour <= 6 || _hour >= 18) {
    $('body').css({'background-image': 'url(./images/night.jpg)'})
  } else {
    $('body').css({'background-image': 'url(./images/day.jpg)'})
  }
}

const generateFalling = () => {
  let date = new Date,
    _hour = date.getHours(),
    fallingImage = _hour <= 6 || _hour >= 18 ? './images/snowflake.png': './images/sun.png';

  for (let i = 0; i < 20; i++) {
    let snowSpan = document.createElement("span");
    snowSpan.classList.add("falling-cursor");

    snowSpan.style.left = Math.floor(Math.random() * $(window).width()) + "px";
    snowSpan.style.top = Math.floor(Math.random() * $(window).height()) + "px";

    image = (i % 2) ? fallingImage : './images/teo.jpg'

    let size = Math.floor(Math.random() * 30);
    snowSpan.style['background-image'] = `url(${image})`;
    snowSpan.style.width = size + "px";
    snowSpan.style.height = size + "px";
    document.body.appendChild(snowSpan);

    setTimeout(() => {
        snowSpan.remove();
    }, 1500);
  }
}

setInterval(() => {
  setTimeKeeper()
}, 1000);

setInterval(() => {
  generateFalling()
}, 200);

generateSecSlash()
generateHourSlashInner()
changeBackground()
generateFalling()
